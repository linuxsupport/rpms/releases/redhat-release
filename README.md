# redhat-release RPM

The main branch of this repo is empty, all the fun happens in other branches:

 * [el8](https://gitlab.cern.ch/linuxsupport/rpms/releases/redhat-release/-/tree/el8)
 * [el9](https://gitlab.cern.ch/linuxsupport/rpms/releases/redhat-release/-/tree/el9)

